class AuxiliarV1 < RuntimeError

  def self.autenticar_usuario(_email, _token, _username, _credencial)
    if _username.present?
			user = User.where(username: _username).first
		else
			user = User.where(email: _email).first
		end

		if user
      credencial_validada = false

      if _credencial.present?
        credencial_validada = user.valid_password?(_credencial)
        user.update(ano_letivo_id: user.ano_letivo_id) if user.authentication_token.nil?
      elsif _token.present?
        credencial_validada = user.authentication_token.eql?(_token)
      end
        
      if credencial_validada
        status = 200
        msg = 'Ok'
      else
        status = 401
        msg = 'Usuário com credencial não validada.'
        user = nil
      end
    else
      status = 401
      msg = 'Usuário não encontrado.'
    end

    return user, status, msg
  end

end