require_relative "boot"

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_mailbox/engine"
require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ApiSvida
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    config.i18n.default_locale = :"pt-BR"
    config.i18n.available_locales = [:"pt-BR"]
    config.i18n.default_locale = :"pt-BR"
    config.time_zone = 'Brasilia'
    config.active_record.default_timezone = :local
    
    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    config.autoload_paths += %W(#{config.root}/../svida/app/models)

    config.middleware.insert_before 0, Rack::Cors do
      allow do
      origins '*'
      resource '*',
      headers: :any,
      methods: %i(get post put patch delete options head)
      end
    end

    config.middleware.use Rack::Attack

    # config.to_prepare do
    #   Devise::SessionsController.layout "empty"
    #   Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? "application" : "empty" }
    #   Devise::ConfirmationsController.layout "empty"
    #   Devise::UnlocksController.layout "empty"
    #   Devise::PasswordsController.layout "empty"
    # end
  end
end
