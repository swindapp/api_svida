Rails.application.routes.draw do
  
  devise_for :users

  namespace :v1 do

    resources :users do
      collection do
        post :autenticar
        patch :atualizar_senha
      end
    end

    resources :turma_avaliacoes do
      collection do
        post :liberadas
        post :disponiveis
        patch :protocolo_abertura
        patch :protocolo_fechamento
        patch :protocolo_abertura_redacao
        patch :gravar_redacao
        patch :protocolo_fechamento_redacao
      end
    end

    resources :turma_avaliacao_questao_respostas do
      collection do
        patch :registrar_resposta
      end
    end

    resources :turma_avaliacao_alunos do
      collection do
        post :painel_respostas
        post :plano_acao_questoes
        post :resumo_materias
      end
    end

    resources :turma_avaliacao_lista_adaptadas do
      collection do
        patch :registrar_resposta
      end
    end

  end

end
