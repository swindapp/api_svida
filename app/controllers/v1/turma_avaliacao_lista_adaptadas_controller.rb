class V1::TurmaAvaliacaoListaAdaptadasController < V1::ApiController
	before_action :set_turma_avaliacao_lista_adaptada, only: [:update, :destroy, :registrar_resposta]

	# GET /api/v1/controllers
	def index
  end

	# GET /api/v1/controllers/1
	def show
		render json: @turma_avaliacao_lista_adaptada
	end

  def registrar_resposta
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      if @turma_avaliacao_lista_adaptada.turma_aluno_id.eql?(params[:turma_aluno_id].to_i)
        if @turma_avaliacao_lista_adaptada.item_resposta.eql?('sem_resposta')
          turma_avaliacao = @turma_avaliacao_lista_adaptada.turma_avaliacao
          if turma_avaliacao.data_correcao_plano_acao.nil?
            _acertou = nil

            case params[:resposta]
            when 'A'
              _item_resposta = 1
            when 'B'
              _item_resposta = 2
            when 'C'
              _item_resposta = 3
            when 'D'
              _item_resposta = 4
            when 'E'
              _item_resposta = 5
            else
              _item_resposta = 6
            end

            unless _item_resposta.eql?(6)
              case @turma_avaliacao_lista_adaptada.avaliacao_conhecimento_questao.gabarito
              when 'opcao_a'
                _gabarito = 1
              when 'opcao_b'
                _gabarito = 2
              when 'opcao_c'
                _gabarito = 3
              when 'opcao_d'
                _gabarito = 4
              when 'opcao_e'
                _gabarito = 5
              end

              _acertou = _item_resposta.eql?(_gabarito)
            end

            @turma_avaliacao_lista_adaptada.update!(item_resposta: _item_resposta, data_registro_resposta: Time.zone.now, acertou: _acertou)

            _status = 200
            _msg = 'Resposta registrada com sucesso.'
          else
            _status = 401
            _msg = 'Plano de ação já finalizado.'
          end
        else
          _status = 401
          _msg = 'Item já respondido.'
        end
      else
        _status = 401
        _msg = 'Item não é a avaliação do aluno'
      end
    end

    render json: { status: _status, msg: _msg, acertou: _acertou }.to_json
  end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_turma_avaliacao_lista_adaptada
		@turma_avaliacao_lista_adaptada = TurmaAvaliacaoListaAdaptada.find(params[:id])
	end

	# Only allow a trusted parameter "white list" through.
	def turma_avaliacao_lista_adaptada_params
		params.require(:turma_avaliacao_lista_adaptada).permit(:name, :email)
	end
end


