
module V1
 class ApiController < ApplicationController
	acts_as_token_authentication_handler_for User
	before_action :require_authentication!
  before_action :configure_permitted_parameters, if: :devise_controller?

	protected
	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:username, :password, :password_confirmation, :email, :name, :pessoa_escola_id, :remember_me])
		devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :password, :password_confirmation])
		devise_parameter_sanitizer.permit(:account_update, keys: [:username, :password, :password_confirmation, :current_password])		
  end  

	private
	def require_authentication!
	  throw(:warden, scope: :user) unless current_user.presence
	end
 end
end
