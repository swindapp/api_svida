class V1::UsersController < V1::ApiController
	before_action :set_user, only: [:update, :destroy]
	# before_action :require_authorization!, only: [:show, :update, :destroy]

	# GET /api/v1/users
	def index
		@users = User.limit(1)
		render json: @users
	end

	# GET /api/v1/users/1
	def show
		render json: @user
	end

	def autenticar
		user, status, msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token] , params[:username], params[:credencial])
		retorno = { status: status, msg: msg }

		if user
			pessoa = user.pessoa
			turma_aluno = pessoa.turma_alunos.joins('inner join turmas on (turma_alunos.turma_id = turmas.id)')
													.where('turmas.ano_letivo_id=?', user.ano_letivo_id)
													.where('turma_alunos.status = ?', TurmaAluno.status[:ativo]).first

			retorno = retorno.merge(
					id: user.id,
					name: user.name,
					username: user.username,
					token: user.authentication_token,
					pessoa_escola_id: user.pessoa_escola_id, 
					nome_escola: user.user_pessoa.nome, 
					pessoa_id: user.pessoa_id, 
					nome_pessoa: user.pessoa.nome, 
					ano_letivo_id: user.ano_letivo_id, 
					ano: user.ano_letivo.ano,
					serie_id: turma_aluno.nil? ? '' : turma_aluno.turma.serie_id,
					serie_nome: turma_aluno.nil? ? '' : turma_aluno.turma.serie.nome,
					nivel_id: turma_aluno.nil? ? '' : turma_aluno.turma.serie.nivel_id,
					nivel: turma_aluno.nil? ? '' : turma_aluno.turma.serie.nivel.nome,
					turma_id: turma_aluno.nil? ? '' : turma_aluno.turma_id,
					turma: turma_aluno.nil? ? '' : turma_aluno.turma.codigo
			)
		end

		render json: retorno.to_json
	end

	def atualizar_senha
		user, status, msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token] , params[:username], params[:credencial])
		retorno = { status: status, msg: msg }

		if user and params[:credencial_nova]
			user.update(password: params[:credencial_nova])
			retorno[:msg] = 'Senha atualizada.'
		end		

		render json: retorno.to_json
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_user
		@user = User.find(params[:id])
	end

end

