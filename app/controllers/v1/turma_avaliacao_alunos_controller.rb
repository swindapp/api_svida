class V1::TurmaAvaliacaoAlunosController < V1::ApiController
	before_action :set_turma_avaliacao_aluno, only: [:update, :destroy, :painel_respostas]

	# GET /api/v1/controllers
	def index
  end

  def painel_respostas
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      turma_avaliacao = @turma_avaliacao_aluno.turma_avaliacao

      if turma_avaliacao.realizado_online
        if params[:area_conhecimento_codigo].present?
          _area_conhecimento = AreaConhecimento.find_by_codigo(params[:area_conhecimento_codigo]).id
        else
          _area_conhecimento = AreaConhecimento.where.not(codigo: 'A03').pluck(:id)
        end

        _painel = []
        turma_avaliacao_questao_respostas = turma_avaliacao.turma_avaliacao_questao_respostas
                                                            .joins('inner join avaliacao_conhecimento_questoes acq on (turma_avaliacao_questao_respostas.avaliacao_conhecimento_questao_id = acq.id)')
                                                            .joins('inner join disciplinas d on (acq.disciplina_id = d.id)')
                                                            .joins('inner join materias m on (d.materia_id = m.id)')
                                                            .where(turma_aluno_id: @turma_avaliacao_aluno.turma_aluno_id)
                                                            .where('m.area_conhecimento_id in (?)', _area_conhecimento)
                                                            .where('acq.lista_adaptada = false')
                                                            .order('acq.numero')

        turma_avaliacao_questao_respostas.each do |turma_avaliacao_questao_resposta|
          avaliacao_conhecimento_questao = turma_avaliacao_questao_resposta.avaliacao_conhecimento_questao
          
          _painel <<  {
              numero_questao: avaliacao_conhecimento_questao.numero,
              resposta: turma_avaliacao_questao_resposta.item_resposta_online_i18n,
              acertou: (turma_avaliacao_questao_resposta.acertou.nil? ? '' : (turma_avaliacao_questao_resposta.acertou? ? 'Sim' : 'Não'))
            }
        end

        unless _painel.empty?
          _status = 200
          _msg = 'Painel de respostas gerado com sucesso'
        else
          _status = 401
          _msg = 'Painel de respostas vazio'
        end
      end
    else
      _status = status = 401
      _msg = 'Avaliação indisponível'
    end
    
    render json: { status: _status, msg: _msg, painel_respostas: _painel }.to_json
  end

	# GET /api/v1/controllers/1
	def show
		render json: @turma_avaliacao_aluno
	end

  def resumo_materias
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      turma_avaliacao = TurmaAvaliacao.find(params[:id])
      if turma_avaliacao.realizado_online
        turma_aluno = TurmaAluno.find(params[:turma_aluno_id])        
        
        if turma_aluno
          area_conhecimento = AreaConhecimento.find_by_codigo(params[:area_conhecimento_codigo])
          turma_avaliacao_aluno = turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno_id: turma_aluno.id).first

          if turma_avaliacao_aluno
            _questoes = []
            _resumo = []
            AreaConhecimento.where.not(codigo: 'A03').order(:ordem_plano_acao).each do |area_conhecimento|
              avaliacao_conhecimento_questoes = turma_avaliacao_aluno.questoes_oficiais_plano_acao(area_conhecimento)
              _questoes.push(avaliacao_conhecimento_questoes.pluck(:id).join(','))
            end

            stSQL = ''
            stSQL << 'select m.area_conhecimento_id, ac.nome_curto as area_conhecimento_nome, ac.codigo as area_conhecimento_codigo, d.materia_id, m.nome as materia_nome, count(*) as qtde_questoes '
            stSQL << 'from avaliacao_conhecimento_questoes acq '
            stSQL << '  inner join disciplinas d on (acq.disciplina_id = d.id) '
            stSQL << '  inner join materias m on (d.materia_id = m.id) '
            stSQL << '  inner join area_conhecimentos ac on (m.area_conhecimento_id = ac.id) '
            stSQL << "where acq.id in (#{ _questoes.join(',') } ) "
            stSQL << 'group by m.area_conhecimento_id, ac.nome_curto, ac.codigo, d.materia_id, m.nome;'

            
            registros = ActiveRecord::Base.connection.execute(stSQL)
            registros.each do |registro|
              _resumo << registro
            end

            unless _resumo.empty?
              _status = 200
              _msg = 'Lista de questões do plano de ação gerada com sucesso'
            else
              _status = 401
              _msg = 'Lista de questões vazia'
            end
          else
            _status = status = 401
            _msg = 'Avaliação indisponível'
          end
        else
          _status = status = 401
          _msg = 'Avaliação indisponível'
        end
      else
        _status = status = 401
        _msg = 'Avaliação indisponível'
      end
    else
      _status = status = 401
      _msg = 'Usuário inválido'
    end
    
    render json: { status: _status, msg: _msg, resumo: _resumo }.to_json
  end

  def plano_acao_questoes
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      turma_avaliacao = TurmaAvaliacao.find(params[:id])
      if turma_avaliacao.realizado_online
        turma_aluno = TurmaAluno.find(params[:turma_aluno_id])        
        
        if turma_aluno
          materia = Materia.find(params[:materia_id])
          turma_avaliacao_aluno = turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno_id: turma_aluno.id).first

          if turma_avaliacao_aluno
            _questoes = []

            avaliacao_conhecimento_questoes = turma_avaliacao_aluno.questoes_oficiais_plano_acao(materia.area_conhecimento)
            avaliacao_conhecimento_questoes.joins('inner join disciplinas d on (avaliacao_conhecimento_questoes.disciplina_id = d.id)').where('d.materia_id =?', materia.id).each do |avaliacao_conhecimento_questao|
              _questoes_plano_acao = []
              _qtd_questoes_plano_acao = 0
              _qtd_respondeu = 0
              _qtd_acertou = 0 

              turma_avaliacao.avaliacao_conhecimento.avaliacao_conhecimento_questoes.where(questao_referencia_id: avaliacao_conhecimento_questao.id).where(lista_adaptada: true).order(:item_plano_acao).each do |acq|
              
                turma_avaliacao_lista_adaptada = turma_avaliacao.turma_avaliacao_lista_adaptadas.where(turma_aluno_id: turma_aluno.id).where(avaliacao_conhecimento_questao_id: acq.id).first
                
                _questoes_plano_acao << {
                  avaliacao_conhecimento_questao_id: acq.id,
                  turma_avaliacao_lista_adaptada_id: turma_avaliacao_lista_adaptada.id,
                  tipo: 'Plano de Ação',
                  item: acq.item_plano_acao_i18n,
                  resposta: turma_avaliacao_lista_adaptada.item_resposta_i18n,
                  gabarito: turma_avaliacao_lista_adaptada.avaliacao_conhecimento_questao.gabarito_i18n,
                  acertou: turma_avaliacao_lista_adaptada.acertou,
                  enunciado: acq.enunciado,
                  item_a: acq.item_1,
                  item_b: acq.item_2,
                  item_c: acq.item_3,
                  item_d: acq.item_4,
                  item_e: acq.item_5
                }

                _qtd_questoes_plano_acao += 1
                unless turma_avaliacao_lista_adaptada.item_resposta.eql?('sem_resposta')
                  _qtd_respondeu += 1 
                  
                  _qtd_acertou += 1 if turma_avaliacao_lista_adaptada.acertou?
                end
              end

              _desempenho = 0
              if (_qtd_questoes_plano_acao > 0) and (_qtd_respondeu.eql?(_qtd_questoes_plano_acao))
                _desempenho = (_qtd_acertou.eql?(_qtd_questoes_plano_acao) ? 1 : 2)
              end

              turma_avaliacao_questao_resposta = avaliacao_conhecimento_questao.turma_avaliacao_questao_respostas.where(turma_avaliacao_id: turma_avaliacao.id).where(turma_aluno_id: turma_avaliacao_aluno.turma_aluno_id).first 

              _questoes <<  {
                  avaliacao_conhecimento_questao_id: avaliacao_conhecimento_questao.id,
                  tipo: 'Avaliação',
                  numero_questao: avaliacao_conhecimento_questao.numero,
                  materia_id: avaliacao_conhecimento_questao.disciplina.materia_id,
                  materia_nome: avaliacao_conhecimento_questao.disciplina.materia.nome,
                  qtd_questoes_plano_acao: _qtd_questoes_plano_acao, 
                  qtd_respondeu: _qtd_respondeu,
                  qtd_acertou: _qtd_acertou,
                  resposta: turma_avaliacao_questao_resposta.item_resposta_i18n,
                  gabarito: avaliacao_conhecimento_questao.gabarito_i18n,
                  desempenho: _desempenho,
                  enunciado: avaliacao_conhecimento_questao.enunciado,
                  item_a: avaliacao_conhecimento_questao.item_1,
                  item_b: avaliacao_conhecimento_questao.item_2,
                  item_c: avaliacao_conhecimento_questao.item_3,
                  item_d: avaliacao_conhecimento_questao.item_4,
                  item_e: avaliacao_conhecimento_questao.item_5,
                  questoes_plano_acao: _questoes_plano_acao
              }
            end

            unless _questoes.empty?
              _status = 200
              _msg = 'Lista de questões do plano de ação gerada com sucesso'
            else
              _status = 401
              _msg = 'Lista de questões vazia'
            end
          else
            _status = status = 401
            _msg = 'Avaliação indisponível'
          end
        else
          _status = status = 401
          _msg = 'Avaliação indisponível'
        end
      else
        _status = status = 401
        _msg = 'Avaliação indisponível'
      end
    else
      _status = status = 401
      _msg = 'Usuário inválido'
    end
    
    render json: { status: _status, msg: _msg, lista_questoes: _questoes }.to_json
  end

  def plano_acao_registrar_resposta
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      if @turma_avaliacao_questao_resposta.turma_aluno_id.eql?(params[:turma_aluno_id].to_i)
        turma_avaliacao = @turma_avaliacao_questao_resposta.turma_avaliacao
        if turma_avaliacao.realizado_online
          retorno = turma_avaliacao.permite_resposta
          
          if retorno[:permite_resposta]
            case params[:resposta]
            when 'A'
              _item_resposta = 1
            when 'B'
              _item_resposta = 2
            when 'C'
              _item_resposta = 3
            when 'D'
              _item_resposta = 4
            when 'E'
              _item_resposta = 5
            else
              _item_resposta = 6
            end

            @turma_avaliacao_questao_resposta.update!(item_resposta_online: _item_resposta, data_registro_resposta: Time.zone.now)

            _status = 200
            _msg = 'Resposta registrada com sucesso'
          else
            _status = 401
            _msg = 'Avaliação não disponível'
          end
        else
          _status = 401
          _msg = 'Avaliação não é online'
        end
      else
        _status = 401
        _msg = 'Item não é a avaliação do aluno'
      end
    end

    render json: { status: _status, msg: _msg }.to_json
  end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_turma_avaliacao_aluno
		@turma_avaliacao_aluno = TurmaAvaliacaoAluno.find(params[:id])
	end

	# Only allow a trusted parameter "white list" through.
	def turma_avaliacao_aluno_params
		params.require(:turma_avaliacao_aluno).permit(:name, :email)
	end
end


