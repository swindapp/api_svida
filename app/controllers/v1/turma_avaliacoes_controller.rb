class V1::TurmaAvaliacoesController < V1::ApiController
	before_action :set_turma_avaliacao, only: [:update, :destroy, :protocolo_abertura, :protocolo_fechamento, :protocolo_abertura_redacao, :protocolo_fechamento_redacao, :gravar_redacao]

	# GET /api/v1/controllers
	def index
  end

	def liberadas
		user, status, msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token] , params[:username], params[:credencial])
		retorno = { status: status, msg: msg }

    if user
      pessoa = Pessoa.find(user.pessoa_id)
    
      turma_aluno = pessoa.turma_alunos.joins('inner join turmas on (turma_alunos.turma_id = turmas.id)')
                                      .where('turmas.ano_letivo_id=?', user.ano_letivo_id)
                                      .where('turma_alunos.status = ?', TurmaAluno.status[:ativo]).first

      if turma_aluno
        turma_avaliacoes = TurmaAvaliacao.joins('inner join avaliacao_conhecimentos on (turma_avaliacoes.avaliacao_conhecimento_id = avaliacao_conhecimentos.id)')
                                          .joins('inner join turma_avaliacao_alunos on (turma_avaliacoes.id = turma_avaliacao_alunos.turma_avaliacao_id)')
                                          .where('turma_avaliacao_alunos.turma_aluno_id=?', turma_aluno.id)
                                          .where('avaliacao_conhecimentos.ano_letivo_id=?', user.ano_letivo_id)
                                          .where('turma_avaliacoes.data_aplicacao <= ?', Date.today)
                                          .where('turma_avaliacoes.status in (9)')
                                          .order('avaliacao_conhecimentos.data_aplicacao')
        avaliacoes = []
        turma_avaliacoes.each do |turma_avaliacao|
          turma_avaliacao_aluno = turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno_id: turma_aluno.id).first
          
          medias = turma_avaliacao_aluno.medias if turma_avaliacao_aluno

          avaliacoes << [
            turma_avaliacao_id: turma_avaliacao.id,
            descricao: turma_avaliacao.descricao,
            notas: {
              tipo_correcao: turma_avaliacao.tri_aplicada? ? 'Correção pela TRI' : 'Correção Clássica',
              linguagens: turma_avaliacao_aluno.nil? ? 0 : medias[:media_linguagens].to_f,
              humanas: turma_avaliacao_aluno.nil? ? 0 : medias[:media_humanas].to_f,
              natureza: turma_avaliacao_aluno.nil? ? 0 : medias[:media_natureza].to_f,
              matematica: turma_avaliacao_aluno.nil? ? 0 : medias[:media_matematica].to_f,
              redacao: turma_avaliacao_aluno.nil? ? 0 : turma_avaliacao_aluno.nota_redacao.to_f
            }
          ]

        end

        if avaliacoes[0]
          retorno = retorno.merge(avaliacoes: avaliacoes)
        else
          retorno = retorno.merge(avaliacoes: 'Sem Avaliação')
        end
      end
    end

    render json: retorno.to_json

	end

  def disponiveis
		user, status, msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token] , params[:username], params[:credencial])
		retorno = { status: status, msg: msg }

    if user
      pessoa = Pessoa.find(user.pessoa_id)
    
      turma_aluno = pessoa.turma_alunos.joins('inner join turmas on (turma_alunos.turma_id = turmas.id)')
                                      .where('turmas.ano_letivo_id=?', user.ano_letivo_id)
                                      .where('turma_alunos.status = ?', TurmaAluno.status[:ativo]).first

      if turma_aluno
        turma_avaliacoes = TurmaAvaliacao.joins('inner join avaliacao_conhecimentos on (turma_avaliacoes.avaliacao_conhecimento_id = avaliacao_conhecimentos.id)')
                                          .joins('inner join turma_avaliacao_alunos on (turma_avaliacoes.id = turma_avaliacao_alunos.turma_avaliacao_id)')
                                          .where('turma_avaliacao_alunos.turma_aluno_id=?', turma_aluno.id)
                                          .where('avaliacao_conhecimentos.ano_letivo_id=?', user.ano_letivo_id)
                                          .where('turma_avaliacoes.data_aplicacao >= ?', Date.today)
                                          .where('turma_avaliacoes.status not in (8,9,10)')
                                          .order('avaliacao_conhecimentos.data_aplicacao')
        avaliacoes = []
        
        turma_avaliacoes.each do |turma_avaliacao|
          area_conhecimento[ [], [], [], [], [] ]

          area_conhecimento[0][turma_avaliacao.dia_aplicacao_linguagem - 1] << 'Linguagens' if turma_avaliacao.dia_aplicacao_linguagem
          area_conhecimento[0][turma_avaliacao.dia_aplicacao_humanas - 1] << 'Humanas' if turma_avaliacao.dia_aplicacao_humanas
          area_conhecimento[0][turma_avaliacao.dia_aplicacao_natureza - 1] << 'Natureza' if turma_avaliacao.dia_aplicacao_natureza
          area_conhecimento[0][turma_avaliacao.dia_aplicacao_matematica - 1] << 'Matemática' if turma_avaliacao.dia_aplicacao_matematica

          avaliacoes << [
            turma_avaliacao_id: turma_avaliacao.id,
            descricao: turma_avaliacao.descricao,
            programacao: [
              {
                dia: 1,
                tipo: 'avaliação',
                hora_inicio: turma_avaliacao.data_hora_inicial_1_dia,
                hora_termino: turma_avaliacao.data_hora_final_1_dia,
                liberada: (Time.zone.now.between?(turma_avaliacao.data_hora_inicial_1_dia || 0, turma_avaliacao.data_hora_final_1_dia || 0)),
                area_conhecimento: area_conhecimento[0]
              },
              {
                dia: 2,
                tipo: 'avaliação',
                hora_inicio: turma_avaliacao.data_hora_inicial_2_dia,
                hora_termino: turma_avaliacao.data_hora_final_2_dia,
                liberada: (Time.zone.now.between?(turma_avaliacao.data_hora_inicial_2_dia || 0, turma_avaliacao.data_hora_final_2_dia || 0)),
                area_conhecimento: area_conhecimento[1]
              },
              {
                dia: 3,
                tipo: 'avaliação',
                hora_inicio: turma_avaliacao.data_hora_inicial_3_dia,
                hora_termino: turma_avaliacao.data_hora_final_3_dia,
                liberada: (Time.zone.now.between?(turma_avaliacao.data_hora_inicial_3_dia || 0, turma_avaliacao.data_hora_final_3_dia || 0)),
                area_conhecimento: area_conhecimento[2]
              },
              {
                dia: 4,
                tipo: 'avaliação',
                hora_inicio: turma_avaliacao.data_hora_inicial_4_dia,
                hora_termino: turma_avaliacao.data_hora_final_4_dia,
                liberada: (Time.zone.now.between?(turma_avaliacao.data_hora_inicial_3_dia || 0, turma_avaliacao.data_hora_final_4_dia || 0)),
                area_conhecimento: area_conhecimento[2]
              }
            ]
          ]
        end

        if avaliacoes[0]
          retorno = retorno.merge(avaliacoes: avaliacoes)
        else
          retorno = retorno.merge(avaliacoes: 'Sem Avaliação')
        end
      end
    end

    render json: retorno.to_json
  end

  def protocolo_abertura
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)
    _protocolo_aberto = false
    _dia = 0
    _turma_aluno_id = nil

    if user
      if @turma_avaliacao.realizado_online
        retorno = @turma_avaliacao.permite_resposta
        
        if retorno[:permite_resposta]
          turma_aluno = TurmaAluno.where(turma_id: @turma_avaliacao.turma_id).where(pessoa_aluno_id: user.pessoa_id).first
          if turma_aluno
            _turma_aluno_id = turma_aluno.id

            turma_avaliacao_aluno = @turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno: turma_aluno.id).first
            if turma_avaliacao_aluno
              _dia = retorno[:dia].to_i
              if retorno[:segunda_chamada]
                case retorno[:dia]
                when 1 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_1_dia, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_1_dia.nil?
                  _protocolo_aberto = true
                when 2 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_2_dia, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_2_dia.nil?
                  _protocolo_aberto = true
                when 3 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_3_dia, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_3_dia.nil?
                  _protocolo_aberto = true
                when 4 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_4_dia, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_4_dia.nil?
                  _protocolo_aberto = true
                when 5 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_5_dia, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_5_dia.nil?
                  _protocolo_aberto = true
                end
              else
                case retorno[:dia]
                when 1 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_1_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_1_dia_2a_chamada.nil?
                  _protocolo_aberto = true
                when 2 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_2_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_2_dia_2a_chamada.nil?
                  _protocolo_aberto = true
                when 3 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_3_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_3_dia_2a_chamada.nil?
                  _protocolo_aberto = true
                when 4 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_4_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_4_dia_2a_chamada.nil?
                  _protocolo_aberto = true
                when 5 
                  turma_avaliacao_aluno.update_column(:data_abertura_online_5_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_abertura_online_5_dia_2a_chamada.nil?
                  _protocolo_aberto = true
                end
              end

              if _protocolo_aberto
                _status = 200
                _msg = 'Protocolo de abertura para responder a avaliação aberto com sucesso'
              else
                _status = 401
                _msg = 'Protocolo não foi aberto'
              end
            else
              _status = status = 401
              _msg = 'Avaliação indisponível para o(a) aluno(a)'
            end
          else
            _status = status = 401
            _msg = 'Aluno não habilitado'
          end
        else
          _status = status = 401
          _msg = 'Avaliação indisponível'
        end
      else
        _status = status = 401
        _msg = 'Avaliação não é online'
      end
    end
    
    render json: { status: _status, msg: _msg, dia: _dia, turma_aluno_id: _turma_aluno_id, turma_avaliacao_aluno_id: turma_avaliacao_aluno.id }.to_json
  end

  def protocolo_fechamento
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)
    _protocolo_fechado = false
    _dia = 0
    _turma_aluno_id = params[:turma_aluno_id].to_i

    if user
      if @turma_avaliacao.realizado_online
        retorno = @turma_avaliacao.permite_resposta
        
        if retorno[:permite_resposta]
          if _turma_aluno_id > 0
            turma_avaliacao_aluno = @turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno: _turma_aluno_id).first
            if turma_avaliacao_aluno
              _dia = retorno[:dia].to_i
              if retorno[:segunda_chamada]
                case retorno[:dia]
                when 1 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_1_dia, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_1_dia.nil?
                  _protocolo_fechado = true
                when 2 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_2_dia, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_2_dia.nil?
                  _protocolo_fechado = true
                when 3 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_3_dia, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_3_dia.nil?
                  _protocolo_fechado = true
                when 4 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_4_dia, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_4_dia.nil?
                  _protocolo_fechado = true
                when 5 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_5_dia, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_5_dia.nil?
                  _protocolo_fechado = true
                end
              else
                case retorno[:dia]
                when 1 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_1_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_1_dia_2a_chamada.nil?
                  _protocolo_fechado = true
                when 2 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_2_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_2_dia_2a_chamada.nil?
                  _protocolo_fechado = true
                when 3 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_3_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_3_dia_2a_chamada.nil?
                  _protocolo_fechado = true
                when 4 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_4_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_4_dia_2a_chamada.nil?
                  _protocolo_fechado = true
                when 5 
                  turma_avaliacao_aluno.update_column(:data_fechamento_online_5_dia_2a_chamada, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_online_5_dia_2a_chamada.nil?
                  _protocolo_fechado = true
                end
              end

              if _protocolo_fechado
                _status = 200
                _msg = 'Protocolo de abertura para responder a avaliação aberto com sucesso'
              else
                _status = 401
                _msg = 'Protocolo não foi aberto'
              end
            else
              _status = status = 401
              _msg = 'Avaliação indisponível para o(a) aluno(a)'
            end
          else
            _status = status = 401
            _msg = 'Aluno não habilitado'
          end
        else
          _status = status = 401
          _msg = 'Avaliação indisponível'
        end
      else
        _status = status = 401
        _msg = 'Avaliação não é online'
      end
    end
    
    render json: { status: _status, msg: _msg, dia: _dia, turma_aluno_id: _turma_aluno_id }.to_json
  end

  def protocolo_abertura_redacao
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)
    _protocolo_aberto = false
    _dia = 0
    _turma_aluno_id = nil

    if user
      if @turma_avaliacao.realizado_online
        retorno = @turma_avaliacao.permite_resposta
        
        if retorno[:permite_resposta]
          turma_aluno = TurmaAluno.where(turma_id: @turma_avaliacao.turma_id).where(pessoa_aluno_id: user.pessoa_id).first
          if turma_aluno
            _turma_aluno_id = turma_aluno.id

            turma_avaliacao_aluno = @turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno: turma_aluno.id).first
            if turma_avaliacao_aluno
              turma_avaliacao_aluno.update_column(:data_abertura_redacao_online, Time.zone.now) if turma_avaliacao_aluno.data_abertura_redacao_online.nil?
              _protocolo_aberto = true
            end

            if _protocolo_aberto
              _status = 200
              _msg = 'Protocolo de abertura para redação realizado com sucesso'
            else
              _status = 401
              _msg = 'Protocolo não foi aberto'
            end
          else
            _status = status = 401
            _msg = 'Avaliação indisponível para o(a) aluno(a)'
          end
        else
          _status = status = 401
          _msg = 'Aluno não habilitado'
        end
      else
        _status = status = 401
        _msg = 'Avaliação indisponível'
      end
    else
      _status = status = 401
      _msg = 'Avaliação não é online'
    end
    
    render json: { status: _status, msg: _msg, turma_aluno_id: _turma_aluno_id }.to_json
  end

  def gravar_redacao
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)
    _redacao_gravada = false
    _dia = 0
    _turma_aluno_id = nil

    if user
      if @turma_avaliacao.realizado_online
        retorno = @turma_avaliacao.permite_resposta
        
        if retorno[:permite_resposta]
          turma_aluno = TurmaAluno.where(turma_id: @turma_avaliacao.turma_id).where(pessoa_aluno_id: user.pessoa_id).first
          if turma_aluno
            _turma_aluno_id = turma_aluno.id

            turma_avaliacao_aluno = @turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno: turma_aluno.id).first
            if turma_avaliacao_aluno
              turma_avaliacao_aluno.update_column(:texto_redacao, params[:texto_redacao])
              _redacao_gravada = true
            end

            if _redacao_gravada
              _status = 200
              _msg = 'Redação gravada com sucesso'
            else
              _status = 401
              _msg = 'Protocolo não foi aberto'
            end
          else
            _status = status = 401
            _msg = 'Avaliação indisponível para o(a) aluno(a)'
          end
        else
          _status = status = 401
          _msg = 'Aluno não habilitado'
        end
      else
        _status = status = 401
        _msg = 'Avaliação indisponível'
      end
    else
      _status = status = 401
      _msg = 'Avaliação não é online'
    end
    
    render json: { status: _status, msg: _msg, turma_aluno_id: _turma_aluno_id }.to_json
  end

  def protocolo_fechamento_redacao
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)
    _protocolo_aberto = false
    _dia = 0
    _turma_aluno_id = nil

    if user
      if @turma_avaliacao.realizado_online
        retorno = @turma_avaliacao.permite_resposta
        
        if retorno[:permite_resposta]
          turma_aluno = TurmaAluno.where(turma_id: @turma_avaliacao.turma_id).where(pessoa_aluno_id: user.pessoa_id).first
          if turma_aluno
            _turma_aluno_id = turma_aluno.id

            turma_avaliacao_aluno = @turma_avaliacao.turma_avaliacao_alunos.where(turma_aluno: turma_aluno.id).first
            if turma_avaliacao_aluno
              turma_avaliacao_aluno.update_column(:data_fechamento_redacao_online, Time.zone.now) if turma_avaliacao_aluno.data_fechamento_redacao_online.nil?
              _protocolo_aberto = true
            end

            if _protocolo_aberto
              _status = 200
              _msg = 'Protocolo de fechamento da redação realizado com sucesso'
            else
              _status = 401
              _msg = 'Protocolo não foi aberto'
            end
          else
            _status = status = 401
            _msg = 'Avaliação indisponível para o(a) aluno(a)'
          end
        else
          _status = status = 401
          _msg = 'Aluno não habilitado'
        end
      else
        _status = status = 401
        _msg = 'Avaliação indisponível'
      end
    else
      _status = status = 401
      _msg = 'Avaliação não é online'
    end
    
    render json: { status: _status, msg: _msg, turma_aluno_id: _turma_aluno_id }.to_json
  end

	# GET /api/v1/controllers/1
	def show
		render json: @turma_avaliacao
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_turma_avaliacao
		@turma_avaliacao = TurmaAvaliacao.find(params[:id])
	end

	# Only allow a trusted parameter "white list" through.
	def turma_avaliacao_params
		params.require(:userturma_avaliacao).permit(:name, :email)
	end
end


