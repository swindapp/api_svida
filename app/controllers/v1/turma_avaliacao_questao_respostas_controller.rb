class V1::TurmaAvaliacaoQuestaoRespostasController < V1::ApiController
	before_action :set_tturma_avaliacao_questao_resposta, only: [:update, :destroy, :registrar_resposta]

	# GET /api/v1/controllers
	def index
  end

  def registrar_resposta
    user, _status, _msg = AuxiliarV1.autenticar_usuario(params[:email], params[:token], params[:username], nil)

    if user
      if @turma_avaliacao_questao_resposta.turma_aluno_id.eql?(params[:turma_aluno_id].to_i)
        turma_avaliacao = @turma_avaliacao_questao_resposta.turma_avaliacao
        if turma_avaliacao.realizado_online
          retorno = turma_avaliacao.permite_resposta
          
          if retorno[:permite_resposta]
            case params[:resposta]
            when 'A'
              _item_resposta = 1
            when 'B'
              _item_resposta = 2
            when 'C'
              _item_resposta = 3
            when 'D'
              _item_resposta = 4
            when 'E'
              _item_resposta = 5
            else
              _item_resposta = 6
            end

            @turma_avaliacao_questao_resposta.update!(item_resposta_online: _item_resposta, data_registro_resposta: Time.zone.now)

            _status = 200
            _msg = 'Resposta registrada com sucesso'
          else
            _status = 401
            _msg = 'Avaliação não disponível'
          end
        else
          _status = 401
          _msg = 'Avaliação não é online'
        end
      else
        _status = 401
        _msg = 'Item não é a avaliação do aluno'
      end
    end

    render json: { status: _status, msg: _msg }.to_json

  end


	# GET /api/v1/controllers/1
	def show
		render json: @turma_avaliacao_questao_resposta
	end

	private
	# Use callbacks to share common setup or constraints between actions.
	def set_tturma_avaliacao_questao_resposta
		@turma_avaliacao_questao_resposta = TurmaAvaliacaoQuestaoResposta.find(params[:id])
	end

	# Only allow a trusted parameter "white list" through.
	def turma_avaliacao_questao_resposta_params
		params.require(:turma_avaliacao_questao_resposta).permit(:name, :email)
	end
end